package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandleException {
    @ExceptionHandler(NullPointerException.class)
    ResponseEntity<String> handleNullPointer(NullPointerException exception) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("error");
    }

    @ExceptionHandler(IllegalArgumentException.class)
    ResponseEntity<String> handleIllegalArgument(IllegalArgumentException exception) {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("repeated answer");
    }
}
