package com.twuc.webApp;

import java.util.*;

public class Game {
    private int id;
    private String answer;

    public Game(int id) {
        this.id = id;
        answer = setAnswer();
    }

    public Game(int id, String answer) {
        this.id = id;
        this.answer = answer;
    }

    public Game() {
    }

    public int getId() {
        return id;
    }

    public String getAnswer() {
        return answer;
    }

    private String setAnswer() {
        List<Integer> numbers = new ArrayList<>();
        StringBuilder answer = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 4; i++) {
            int randomNumber = random.nextInt(10);
            while (numbers.contains(randomNumber)) {
                randomNumber = random.nextInt(10);
            }
            numbers.add(randomNumber);
            answer.append(randomNumber);
        }
        return answer.toString();
    }
}
