package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class GameController {
    private int id;
    private List<Game> games = new ArrayList<>();

    @GetMapping("/games")
    ResponseEntity createGame() {
        id++;
        games.add(new Game(id));
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("location", "/api/games/" + id)
                .body("");
    }

    @GetMapping("/games/{gameId}")
    ResponseEntity<Game> getGameStatus(@PathVariable int gameId) {
        List<Game> gameLists = GameService.findGame(gameId, games);
        if (gameLists.size() == 0) {
            throw new NullPointerException();
        }
        return ResponseEntity.ok().body(gameLists.get(0));
    }

    @PostMapping("/games/{gameId}")
    ResponseEntity<GameResult> getGameResult(@PathVariable int gameId, @RequestBody @Valid Answer answer) {
        List<Game> gameLists = GameService.findGame(gameId, games);
        if (gameLists.size() == 0) {
            throw new NullPointerException();
        }
        if (!GameService.isRepeated(answer)) {
            throw new IllegalArgumentException();
        }
        return ResponseEntity.ok().body(GameService.getResult(gameId, games, answer));
    }
}
