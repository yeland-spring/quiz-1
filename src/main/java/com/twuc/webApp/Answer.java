package com.twuc.webApp;

import javax.validation.constraints.Pattern;

public class Answer {
    @Pattern(regexp = "[0-9]{4}")
    private String content;

    public Answer(String content) {
        this.content = content;
    }

    public Answer() {
    }

    public String getContent() {
        return content;
    }
}
