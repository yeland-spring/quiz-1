package com.twuc.webApp;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GameService {
    public static List<Game> findGame(int gameId, List<Game> games) {
        return games.stream().filter(game -> game.getId() == gameId).collect(Collectors.toList());
    }

    public static GameResult getResult(int gameId, List<Game> games, Answer answer) {
        Game game = findGame(gameId, games).get(0);
        boolean correct = false;
        String[] givenAnswer = answer.getContent().split("");
        long ANumber = Arrays.stream(givenAnswer)
                .filter(item -> answer.getContent().indexOf(item) == game.getAnswer().indexOf(item))
                .count();
        long BNumber = Arrays.stream(givenAnswer)
                .filter(item -> game.getAnswer().contains(item) &&
                        answer.getContent().indexOf(item) != game.getAnswer().indexOf(item))
                .count();
        if (ANumber == 4 && BNumber == 0) {
            correct = true;
        }
        String hint = String.format("%dA%dB",ANumber, BNumber);
        return new GameResult(hint, correct);
    }

    public static boolean isRepeated(Answer answer) {
        String[] numbers = answer.getContent().split("");
        Stream<String> stream = Arrays.stream(numbers).filter(number ->
                answer.getContent().indexOf(number) != answer.getContent().lastIndexOf(number));
        return stream.count() == 0;
    }
}
