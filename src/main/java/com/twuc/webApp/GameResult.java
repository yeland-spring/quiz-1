package com.twuc.webApp;

public class GameResult {
    private String hint;
    private boolean correct;

    public GameResult(String hint, boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public GameResult() {
    }

    public String getHint() {
        return hint;
    }

    public boolean isCorrect() {
        return correct;
    }
}
