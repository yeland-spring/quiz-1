package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.client.AutoConfigureWebClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebClient
class GameIntegrationTest {
    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    void should_return_201_with_header_when_create_game() {
        ResponseEntity<Void> entity = testRestTemplate.getForEntity("/api/games", Void.class);
        assertEquals(HttpStatus.CREATED, entity.getStatusCode());
        assertEquals("/api/games/1", entity.getHeaders().get("location").get(0));
    }

    @Test
    void should_return_game_status_when_request() {
        testRestTemplate.getForEntity("/api/games", Void.class);
        ResponseEntity<Game> entity = testRestTemplate.getForEntity("/api/games/1", Game.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(4, entity.getBody().getAnswer().length());
    }

    @Test
    void should_return_status_when_request_with_answer() {
        testRestTemplate.getForEntity("/api/games", Void.class);
        ResponseEntity<GameResult> entity = testRestTemplate.postForEntity("/api/games/1", new Answer("1234"), GameResult.class);
        assertEquals(HttpStatus.OK, entity.getStatusCode());
        assertEquals(MediaType.APPLICATION_JSON_UTF8, entity.getHeaders().getContentType());
        assertEquals(Boolean.FALSE, entity.getBody().isCorrect());
    }

    @Test
    void should_return_404_when_gameId_is_null() {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/games/4", String.class);
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }

    @Test
    void should_return_404_when_post_gameId_is_null() {
        ResponseEntity<String> entity = testRestTemplate.postForEntity("/api/games/4", new Answer("1234"), String.class);
        assertEquals(HttpStatus.NOT_FOUND, entity.getStatusCode());
    }
    @Test
    void should_return_400_when_gameId_is_not_number() {
        ResponseEntity<String> entity = testRestTemplate.getForEntity("/api/games/a", String.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }

    @Test
    void should_return_400_when_answer_is_disabled() {
        testRestTemplate.getForEntity("/api/games", Void.class);
        ResponseEntity<GameResult> entity = testRestTemplate.postForEntity("/api/games/1", new Answer("123"), GameResult.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }

    @Test
    void should_return_400_when_answer_is_not_number() {
        testRestTemplate.getForEntity("/api/games", Void.class);
        ResponseEntity<GameResult> entity = testRestTemplate.postForEntity("/api/games/1", new Answer("123a"), GameResult.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }

    @Test
    void should_return_400_when_answer_is_repeated() {
        testRestTemplate.getForEntity("/api/games", Void.class);
        ResponseEntity<String> entity = testRestTemplate.postForEntity("/api/games/1", new Answer("1231"), String.class);
        assertEquals(HttpStatus.BAD_REQUEST, entity.getStatusCode());
    }
}
