package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameServiceTest {
    @Test
    void should_find_game_from_list() {
        Game game = new Game(2, "3456");
        Game antherGame = new Game(3, "1256");
        List<Game> gameList = Arrays.asList(game, antherGame);
        List<Game> expectList = Arrays.asList(game);
        List<Game> actualList = GameService.findGame(2, gameList);
        assertEquals(expectList.get(0).getId(), actualList.get(0).getId());
        assertEquals(expectList.get(0).getAnswer(), actualList.get(0).getAnswer());
    }

    @Test
    void should_return_result_given_answer() {
        Game game = new Game(2, "3456");
        Game antherGame = new Game(3, "1256");
        List<Game> gameList = Arrays.asList(game, antherGame);
        Answer answer = new Answer("3245");
        GameResult expectResult = new GameResult("1A2B", false);
        GameResult actualResult = GameService.getResult(2, gameList, answer);
        assertEquals(expectResult.getHint(), actualResult.getHint());
        assertEquals(expectResult.isCorrect(), actualResult.isCorrect());
    }

    @Test
    void should_return_correct_when_answer_is_correct() {
        Game game = new Game(2, "3456");
        Game antherGame = new Game(3, "1256");
        List<Game> gameList = Arrays.asList(game, antherGame);
        Answer answer = new Answer("3456");
        GameResult expectResult = new GameResult("4A0B", true);
        GameResult actualResult = GameService.getResult(2, gameList, answer);
        assertEquals(expectResult.getHint(), actualResult.getHint());
        assertEquals(expectResult.isCorrect(), actualResult.isCorrect());
    }

    @Test
    void should_return_false_when_answer_is_repeated() {
        Answer answer = new Answer("2324");
        boolean actual = GameService.isRepeated(answer);
        assertEquals(Boolean.FALSE, actual);
    }

    @Test
    void should_return_true_when_answer_is_legal() {
        Answer answer = new Answer("2364");
        boolean actual = GameService.isRepeated(answer);
        assertEquals(Boolean.TRUE, actual);
    }
}